# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "CAD parts",
    "author": "Thierry Dassé [Thed, BUG de Paris]",
    "version": (0, 2, 0),
    "blender": (2, 80, 0),
    "location": "View3D > Add > Mesh",
    "description": "Add CAD parts",
    "warning": "",
    "doc_url": "https://framagit.org/makeforartandscience/cad-parts/-/wikis/home",
    "category": "Add Mesh",
}

if "bpy" in locals():
    import importlib
    importlib.reload(cad_gears)
else:
    from . import cad_gears

import bpy

class CADPartsMenu(bpy.types.Menu):
    bl_label = "CAD parts"	   
    bl_idname = "VIEW3D_MT_mesh_CADPartsMenu_add"
        
    def draw(self, context):
        self.layout.operator(cad_gears.Gear.bl_idname)
        self.layout.operator(cad_gears.HelixGear.bl_idname)
   
def CADPartsMenuAdd(self, context):
    self.layout.menu(CADPartsMenu.bl_idname)
   
    
def register():
    from bpy.utils import register_class
    
    bpy.utils.register_class(cad_gears.Gear)
    bpy.utils.register_class(cad_gears.HelixGear)
    bpy.utils.register_class(CADPartsMenu)
    bpy.types.VIEW3D_MT_mesh_add.append(CADPartsMenuAdd)

def unregister():
    from bpy.utils import register_class
    
    bpy.utils.unregister_class(cad_gears.Gear)
    bpy.utils.unregister_class(cad_gears.HelixGear)
    bpy.utils.unregister_class(CADPartsMenu)
    bpy.types.VIEW3D_MT_mesh_add.remove(CADPartsMenuAdd)

if __name__ == "__main__":
    register()

