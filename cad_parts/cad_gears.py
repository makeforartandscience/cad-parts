import bpy
from math import pi, cos, sin, tan, sqrt, atan2
from mathutils import Vector, Matrix

def _polar_angle(v):
    
    r = v[0]**2+v[1]**2
    if r == 0:
        return 0
    else:
        return atan2(v[1],v[0])

def _dist(u,v):
    return sqrt((u[0]-v[0])**2+(u[1]-v[1])**2+(u[2]-v[2])**2)

def _angle(u,v):
    return _polar_angle((v[0]-u[0],v[1]-u[1],v[2]-u[2]))

def _tr(v,a):
    """ """
    return (v[0]+a[0],v[1]+a[1],v[2]+a[2])

def _rotZ(v,a,c=(0,0,0)):
    """ rotate vertice v angle a """
    return _tr(((v[0]-c[0])*cos(a)-(v[1]-c[1])*sin(a),(v[0]-c[0])*sin(a)+(v[1]-c[1])*cos(a),v[2]-c[2]),c)

def _sY(v):
    return (v[0],-v[1],v[2])

def addGear(context,z,m,center_radius,height,alpha,level,tb,beta=0,double=False):
    
    verts = []
    faces = []
                    
    # définitions des rayons
    m = m/cos(beta) # module normal
    r = m*z/2 # rayon primitif
    r_b = r*cos(alpha) # rayon de base
    r_i = r - 1.25*m # rayon du pied de la denture (intérieur)
    r_e = r + m # rayon de la tête de la denture (extérieur)            
        
    # calcul des angles
    t_a = sqrt(1/cos(alpha)**2-1) # angle de la développante
    t_b = sqrt((r_e/r_b)**2-1) # angle d'arrivée de la développante

    i_r = -pi/2/z
    i_a = _polar_angle((r_b*(cos(t_a)+t_a*sin(t_a)),r_b*(sin(t_a)-t_a*cos(t_a))))

    # rayon interne
    verts.append((center_radius*cos(i_r),center_radius*sin(i_r),height/2))
    # base de la dent
    o = _rotZ((r_i,0,height/2),-pi/(2*z)-i_a) # point contact interieur
    if tb < 0.01:
        verts.append(o)
        div = 2*level+2
    else:
        b = _rotZ((r_b,0,height/2),-pi/(2*z)-i_a) # point de la base de la developpante
        u = _rotZ(_sY(o),-2*pi/z) # point contact intérieur dent précédente
        rbev = min(0.9*_dist(o,b),0.45*_dist(o,u),tb*m) # rayon arrondi
        c = _tr(_rotZ((rbev/sin((_angle(o,b)-_angle(o,u))/2),0,0),(_angle(o,b)+_angle(o,u))/2),o) #centre arrondi
        a_t = pi/2-abs((_angle(o,b)-_angle(o,u))/2) # angle tangence
        a_c = pi+(_angle(o,b)+_angle(o,u))/2 # angle vecteur c,o
        for i in range(1+level):
            verts.append(_tr(_rotZ((rbev,0,0),a_c+(2*(level-i)/level-1)*a_t),c))
        div = 3*level+2
        
    # développante de cercle
    for i in range(2*level):
        t = t_b*sqrt(i/(2*level-1))
        verts.append(_rotZ((r_b*(cos(t)+t*sin(t)),r_b*(sin(t)-t*cos(t)),height/2),-pi/(2*z)-i_a))

    # partie symétrique
    for i in range(div):
        verts.append((verts[i][0],-verts[i][1],verts[i][2]))
        
    # rotation des dents    
    for t in range(1,z):
        for i in range(2*div):
            verts.append(_rotZ(verts[i],t*2*pi/z))
            
    n = 2*div*z

    # génération des loops
    if abs(beta) > 1e-5:
        # roue helicoidale
        loops = 2*level*(int(height/m)//10 +1)+1
        b_z = height*tan(beta)/2/r # angle helicoidale
        for loop in range(1,loops):
            h = round((1-2*loop/(loops-1))*height/2,5)
            if double:
                r_z = abs(1-2*loop/(loops-1))*b_z
            else:
                r_z = (1-2*loop/(loops-1))*b_z
            for i in range(n):
                verts.append(_rotZ((verts[i][0],verts[i][1],h),r_z))
        for i in range(n):
            verts[i] = _rotZ(verts[i],b_z)
    else:
        # roue droite 
        loops = 2           
        # symétrie par rapport au plan XY  
        for i in range(n):
            verts.append((verts[i][0],verts[i][1],-verts[i][2]))
    
    # faces du dessus 
    for t in range(z):
        r = 2*div*t 
        for i in range(div-1):
            faces.append((r+i,r+i+1,r+i+div+1,r+i+div))
        faces.append((r+div,r+div+1,(r+2*div+1)%n,(r+2*div)%n))

    # faces du dessous
    for t in range(z):
        r = 2*div*t 
        o = n*(loops-1)
        for i in range(div-1):
            faces.append((o+r+i,o+r+i+div,o+r+i+div+1,o+r+i+1))
        faces.append((o+r+div,o+(r+2*div)%n,o+(r+2*div+1)%n,o+r+div+1))

    # faces latérales intérieures
    for loop in range(loops-1):  
        o = n*loop  
        for t in range(z):
            r = 2*div*t             
            faces.append((o+r,o+r+div,o+n+r+div,o+n+r))
            faces.append((o+r+div,o+(r+2*div)%n,o+n+(r+2*div)%n,o+n+r+div))

    # faces latérales extérieures
    for loop in range(loops-1):  
        o = n*loop  
        for t in range(z):
            r = 2*div*t 
            for i in range(1,div-1):
                faces.append((o+r+i+1,o+r+i,o+n+r+i,o+n+r+i+1))
                faces.append((o+r+div+i+1,o+n+r+div+i+1,o+n+r+div+i,o+r+div+i))
            faces.append((o+r+div-1,o+n+r+div-1,o+n+r+2*div-1,o+r+2*div-1)) # extremité de la dent
            faces.append((o+r+div+1,o+n+r+div+1,o+n+(r+2*div+1)%n,o+(r+2*div+1)%n)) # interieur de la dent

    if context.mode == 'OBJECT':
            
        bpy.ops.object.select_all(action='DESELECT')
        mesh = bpy.data.meshes.new("gear_mesh")
        obj = bpy.data.objects.new("gear", mesh)
        bpy.context.scene.collection.objects.link(obj)
        obj.location = bpy.context.scene.cursor.location
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj

        mesh.from_pydata(verts, [], faces)

    elif context.mode == 'EDIT_MESH':
            
        bpy.ops.mesh.select_all(action='DESELECT')
        active_object = context.active_object
        name_active_object = active_object.name
            
        bpy.ops.object.mode_set(mode='OBJECT')
        mesh = bpy.data.meshes.new("gear_mesh")
        mesh_name = mesh.name
        obj = bpy.data.objects.new("gear", mesh)
        bpy.context.scene.collection.objects.link(obj)
        obj.location = bpy.context.scene.cursor.location
        obj.select_set(True)
        mesh.from_pydata(verts, [], faces)

        active_object.select_set(True)
        bpy.ops.object.join()
        context.active_object.name = name_active_object
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.data.meshes.remove(obj.data)
            
    return {'FINISHED'}
    
class HelixGear(bpy.types.Operator):
    """Object Helix Gear"""
    bl_idname = "mesh.helixgear"
    bl_label = "Helix Gear"
    bl_options = {'REGISTER', 'UNDO'}

    teeth: bpy.props.IntProperty(name="Teeths", default=6, min=3, max=240)
    module: bpy.props.FloatProperty(name="Module", default=0.001, min=0.0001, max=10,step = 0.001,description="Gear's module",unit='LENGTH')
    radius: bpy.props.FloatProperty(name="Center radius", default=0.001, min=0.0001, max=10,step = 0.001,description="Center hole radius",unit='LENGTH')
    thickness: bpy.props.FloatProperty(name="Thickness", default=0.004, min=0.0001,step = 0.001,max=10,description="Gear's thickness",unit='LENGTH')
    alpha: bpy.props.FloatProperty(name="Pressure angle", default=20*pi/180, min=10*pi/180, max=30*pi/180,step=100,description="Pressure angle",unit='ROTATION')
    gtype: bpy.props.EnumProperty(name="Helix type",items=[('single','single','single helix'),('double','double','double helix')],default='single', description='Helix type')
    beta: bpy.props.FloatProperty(name="Helix angle", default=0, min=-60*pi/180, max=60*pi/180,step=100,description="Helix angle",unit='ROTATION')
    tbev: bpy.props.FloatProperty(name="Teeth Bevel", default=0.2, min=0., max=0.5,step = 10,description="Teeth bevel")
    level: bpy.props.IntProperty(name="Level", default=2, min=1, max=5)
    
    def execute(self, context):
        
        return addGear(context,self.teeth,self.module,self.radius,self.thickness,self.alpha,self.level,self.tbev,self.beta,double=self.gtype == 'double')
    
class Gear(bpy.types.Operator):
    """Object Gear"""
    bl_idname = "mesh.gear"
    bl_label = "Gear"
    bl_options = {'REGISTER', 'UNDO'}

    teeth: bpy.props.IntProperty(name="Teeths", default=6, min=3, max=240)
    module: bpy.props.FloatProperty(name="Module", default=0.001, min=0.0001, max=10,step = 0.001,description="Gear's module",unit='LENGTH')
    radius: bpy.props.FloatProperty(name="Center radius", default=0.001, min=0.0001, max=10,step = 0.001,description="Center hole radius",unit='LENGTH')
    thickness: bpy.props.FloatProperty(name="Thickness", default=0.004, min=0.0001,step = 0.001,max=10,description="Gear's thickness",unit='LENGTH')
    alpha: bpy.props.FloatProperty(name="Pressure angle", default=20*pi/180, min=15*pi/180, max=30*pi/180,step=100,description="Pressure angle",unit='ROTATION')
    tbev: bpy.props.FloatProperty(name="Teeth Bevel", default=0.2, min=0., max=0.5,step = 10,description="Teeth bevel")
    level: bpy.props.IntProperty(name="Level", default=2, min=1, max=5)
    
    def execute(self, context):
       
        return addGear(context,self.teeth,self.module,self.radius,self.thickness,self.alpha,self.level,self.tbev)

